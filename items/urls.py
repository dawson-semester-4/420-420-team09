from django.urls import path
from . import views

app_name = 'item'

# All the possible url paths related to items
urlpatterns = [
    path('', views.customerItems, name='home'),
    path('additems/', views.addItems),
    path('<int:pk>', views.ViewItem.as_view(), name='view_item'),
    path('<int:pk>/like', views.likeItem, name='like_item'),
    path('<int:pk>/rate', views.rateItem, name='rate_item'),
    path('<int:pk>/modifyItem/', views.ModifyItem.as_view(), name="modify_item"),
    path('<int:pk>/deleteItem/', views.DeleteItem.as_view(template_name='items/deleteitem.html'), name="delete_item"),
    path('<int:pk>/buyItem/', views.BuyItem.as_view(), name="buy_item"),
]
