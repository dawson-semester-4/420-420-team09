from login.models import UserProfile
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

# Creating the Item model with its required attributes


class Item(models.Model):
    img = models.ImageField(upload_to='images/', default="images/default.jpg")
    title = models.CharField(max_length=200)
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    description = models.TextField(max_length=6000, default='')
    sold = models.BooleanField(default=False)
    likes = models.IntegerField(default=0)
    rate = models.DecimalField(max_digits=2, decimal_places=1, default=0)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

# Creating the LogItem model with its required attributes


class LogItem(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    creationDate = models.DateTimeField('created', default=timezone.now())
    lastModified = models.DateTimeField('modified', default=timezone.now())
