import decimal
from django.forms.utils import flatatt
from django.urls import reverse
from django.contrib.auth import logout
from django.forms.models import BaseModelForm, BaseModelFormSet
from django.http.response import HttpResponseRedirect
from login.models import UserProfile
from django.views import generic
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import redirect
from items.models import Item, LogItem
from .forms import AddItemForm
from django.views.generic.edit import DeleteView, UpdateView
from django.utils import timezone

# Method to


def getUser(req):
    user = req.user
    if user.is_authenticated:
        userprofile = UserProfile.objects.get(user_id=user.id)
        return {'userprofile': userprofile}
    return {}

# Method to display all the items as a guest


def guestItems(request):
    logout(request)
    items = Item.objects.all().order_by('-likes')
    if request.method == "POST":
        if len(request.POST.get('search')) != 0:
            items = Item.objects.filter(title__contains=request.POST.get('search')).order_by('-likes')
    context = {'items': items}
    template = loader.get_template('items/guestitems.html')
    return HttpResponse(template.render(context, request))

# Method to display all the items as a customer with additional functions


def customerItems(request):
    user = request.user
    if user.is_authenticated:
        items = Item.objects.all().order_by('-likes')
        if request.method == "POST":
            if len(request.POST.get('search')) != 0:
                items = Item.objects.filter(title__contains=request.POST.get('search')).order_by('-likes')
        username = request.user.username
        context = {"items": items, "username": username}
        context.update(getUser(request))
        template = loader.get_template('items/customeritems.html')
        return HttpResponse(template.render(context, request))
    return redirect('/')

# Method to add items to the database as a customer only


def addItems(request):
    form = AddItemForm(request.POST or None, request.FILES or None)
    user = request.user

    if not user.is_authenticated:
        return redirect('/home')

    if form.is_valid():
        name = form.cleaned_data.get('name')
        price = form.cleaned_data.get('price')
        description = form.cleaned_data.get('description')
        image = form.cleaned_data.get('image')
        Item.objects.create(
            title=name,
            price=price,
            description=description,
            img=image,
            user=user
        )
        LogItem.objects.create(
            title=name,
            author=user,
            creationDate=timezone.now(),
            lastModified=timezone.now())
        return redirect('/items')

    context = {'form': form}
    context.update(getUser(request))
    template = loader.get_template('items/additems.html')
    return HttpResponse(template.render(context, request))

# Method to view an individual items with additional information


class ViewItem(generic.DetailView):
    model = Item
    template_name = 'items/item.html'

    def get(self, request, pk):
        item = Item.objects.get(id=pk)
        context = {"item": item}
        context.update(getUser(request))
        template = loader.get_template('items/item.html')
        return HttpResponse(template.render(context, request))

# Method to buy an item, change the owner of that item, set to sold, transfer the balance amounts, save it to the database and redirect the user to the items page


class BuyItem(UpdateView):
    model = Item
    fields = ['sold', 'user']
    template_name = 'items/buyitem.html'

    def get(self, request, pk):
        user = request.user
        if not user.is_authenticated:
            return redirect('/home')
        item = Item.objects.get(id=pk)
        context = {"item": item, 'sufficientBalance': True}
        context.update(getUser(request))
        template = loader.get_template('items/buyitem.html')
        return HttpResponse(template.render(context, request))

    def post(self, request, pk, *args, **kwargs) -> HttpResponse:
        item = Item.objects.get(id=pk)
        user = UserProfile.objects.get(user_id=request.user.id)
        user2 = UserProfile.objects.get(user_id=item.user_id)
        if user.balance >= item.price:
            item.user_id = user.user_id
            item.sold = True
            user.balance -= item.price
            user2.balance += item.price
            item.save()
            user.save()
            user2.save()
            return redirect('/items/')
        context = {"item": item, 'sufficientBalance': False}
        context.update(getUser(request))
        template = loader.get_template('items/buyitem.html')
        return HttpResponse(template.render(context, request))
        # return HttpResponseRedirect(reverse('item:buy_item', args=[str(pk)]))

# Method to like an item and save it to the database


def likeItem(request, pk):
    item = get_object_or_404(Item, id=request.POST.get('like'))
    item.likes += 1
    item.save()
    return redirect('/items/' + str(pk))

# Method to rate an item and save it to the database


def rateItem(request, pk):
    item = get_object_or_404(Item, id=request.POST.get('ratebtn'))
    if item.rate != 0:
        item.rate = (item.rate + decimal.Decimal(float(request.POST.get('rate')))) / 2
    else:
        item.rate = request.POST.get('rate')
    item.save()
    return redirect('/items/' + str(pk))

# Method to delete an item from the database


class DeleteItem(DeleteView):
    model = Item
    success_url = "/items"

# Method to modify an item in the database


class ModifyItem(UpdateView):
    model = Item
    fields = ['title', 'price', 'description']
    template_name = 'items/modifyitem.html'

    def form_valid(self, form: BaseModelForm) -> HttpResponse:
        pk = self.get_object().pk
        item = Item.objects.get(id=pk)
        if form.is_valid():
            item.title = form.cleaned_data.get('title')
            item.price = form.cleaned_data.get('price')
            item.description = form.cleaned_data.get('description')
            item.save()
            return redirect('/items/' + str(pk))
        return super().form_valid(form)
