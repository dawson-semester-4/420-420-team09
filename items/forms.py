from django import forms
from django.db import models
from .models import Item

# Form to add items to the database


class AddItemForm(forms.Form):
    name = forms.CharField(label="Item Name:", max_length=200)
    price = forms.CharField(label="Item Price:", max_length=200)
    description = forms.CharField(label="Description:", max_length=200)
    image = forms.ImageField(label="Item Image")

    class Meta:
        model = Item
        fields = ('name', 'price', 'description', 'image')

# Form to modify items on the database


class ModifyItemForm(forms.Form):
    title = forms.CharField(label="Title")
    price = forms.DecimalField(label="Price")
    description = forms.CharField(label="Description")
