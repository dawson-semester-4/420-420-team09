from login.models import UserProfile
from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from .forms import UpdateAvatarForm, UpdateBalanceForm, ResetPasswordForm
from django.shortcuts import redirect

# Method to nagivate to the profile view


def profileView(request):
    form = UpdateAvatarForm(request.POST or None, request.FILES or None)
    user = request.user

    userprofile = UserProfile.objects.get(user_id=user.id)
    if request.method == 'POST':
        if form.is_valid():
            img = form.cleaned_data.get('avatar')
            userprofile.avatar = img
            userprofile.save()
    context = {"user": user, 'userprofile': userprofile, "form": form}
    template = loader.get_template('userprofile/profile.html')
    return HttpResponse(template.render(context, request))

# Method to navigate to the page to add balance on an accou


def balanceView(request):
    form = UpdateBalanceForm(request.POST or None, request.FILES or None)
    user = request.user
    userprofile = UserProfile.objects.get(user_id=user.id)
    if request.method == 'POST':
        if form.is_valid():
            print(user.password)
            balance = form.cleaned_data.get('balance')
            userprofile.balance += balance
            userprofile.save()
            return redirect('/profile')
    context = {"user": user, 'userprofile': userprofile, "form": form}
    template = loader.get_template('userprofile/addBalance.html')
    return HttpResponse(template.render(context, request))

# Method to reset the password of an account on the database


# def resetPassword(request):
#     form = ResetPasswordForm(request.POST or None, request.FILES or None)
#     user = request.user
#     userprofile = UserProfile.objects.get(user_id=user.id)
#     if request.method == 'POST':
#         if form.is_valid():
#             newpass = form.cleaned_data.get('new_passwd')
#             user.password = newpass
#             user.save()
#             return redirect('/profile')
#     context = {"user": user, 'userprofile': userprofile, "form": form}
#     template = loader.get_template('userprofile/resetPassword.html')
#     return HttpResponse(template.render(context, request))
