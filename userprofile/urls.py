from django.urls import path

from django.contrib.auth import views as auth_views
from . import views

# All the possible url paths related to userprofile
urlpatterns = [
    path('', views.profileView, name="profileView"),
    path('addBalance/', views.balanceView, name="profileView"),
    # path('resetPassword/', auth_views.PasswordResetView.as_view(template_name="userprofile/password_reset.html", email_template_name='accounts/registration/password_reset_email.html'), name="resetPass"),
    # path('resetPassword_sent/', auth_views.PasswordResetDoneView.as_view(template_name="userprofile/password_reset_done.html"), name="resetPassDone"),
    # path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name="userprofile/password_reset_confirm.html"), name="resetPassConfirm"),
    # path('resetPassword_complete/', auth_views.PasswordResetCompleteView.as_view(template_name="userprofile/password_reset_complete.html"), name="resetPassComplete"),

]
