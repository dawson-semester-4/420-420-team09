from django import forms
from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

# Form to update the avatar image of a user in the database


class UpdateAvatarForm(forms.Form):
    avatar = forms.ImageField(label="avatar")

# Form to update the balance of a user in the database


class UpdateBalanceForm(forms.Form):
    balance = forms.DecimalField(label="Balance")

# Form to reset the password of a user in the database


class ResetPasswordForm(forms.Form):
    new_passwd = forms.CharField(label="New password")
