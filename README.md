### 420-420-team09

# Kijiji Clone using Python and Django

# Members : Neil Fisher, Mate Barabas, Jei Wen Wu, Christian Bula

# Example image
![](siteImageExample.png)

# Steps to run the program:

1. Login using one of the customers accounts or sign up
2. Add balance to that account
3. Shop away

# Heroku website: https://fakijiji.herokuapp.com/

# How to access admin page:

## https://fakijiji.herokuapp.com/admin

or

## localhost:8000/admin

Username: admin
Password: admin

## Names/Password of all users:

Username: Joe
Password: Dawson

Username: Jei
Password: Dawson

Username: Laurent
Password: Dawson

Username: Mate
Password: Dawson

Username: LilGoatie
Password: dawson

Username: neilf
Password: neilf

How to start the web site if unable to run Heroku:

Details:

Everyone that do not have an account are considered 'visitors'

Comments:
When the default page was loaded on Heroku, it was going to the right directory, but it was looking for the wrong file: guestitems.html instead of guestItems.html.
For some reason, this didn't matter locally and it loaded the page regardless of the capitalization. It took several days to figure out the problem.

Whenever a user tried to access the database on Heroku, there was an error saying something like 'table does not exist'. When uploading to Heroku and migrating after using the CLI, the tables were still never made. I (Neil) had to use pgadmin4 to _backup_ the tables from the local database and _restore_ them to the database on heroku. I tried using a command for this though the heroku CLI, but it said something like 'insifficient permissions'.
