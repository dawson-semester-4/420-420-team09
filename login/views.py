from login.models import UserProfile
from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from .forms import SignUpForm, LoginForm
from django.shortcuts import redirect
from django import forms
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.utils import timezone

# Method to navigate to the home page


def home(request):
    context = {}
    template = loader.get_template('login/home.html')
    return HttpResponse(template.render(context, request))

# Method to navigate to the login page


def loginView(request):
    form = LoginForm(request.POST or None)
    context = {'form': form, 'nouser': False}
    if request.method == 'POST':
        # Checks that the form is not empty but not if its valid
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('/items/')
            else:
                context['nouser'] = True
    template = loader.get_template('login/login.html')
    return HttpResponse(template.render(context, request))

# Method to navigate to the signup page


def signupView(request):
    form = SignUpForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        newuser = User.objects.create_user(username=form.cleaned_data.get('username'),
                                           email=form.cleaned_data.get('email'),
                                           password=form.cleaned_data.get('password'))
        newuser.first_name = form.cleaned_data.get('fname')
        newuser.last_name = form.cleaned_data.get('lname')
        newuser.save()
        user = UserProfile.objects.create(user=newuser, avatar=form.cleaned_data.get('avatar'), date_of_birth=form.cleaned_data.get('date_of_birth'))
        user.save()
        login(request, newuser)
        return redirect('/items')
    context = {'form': form}
    template = loader.get_template('login/signup.html')
    return HttpResponse(template.render(context, request))
