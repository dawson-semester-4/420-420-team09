from django.urls import path
from . import views

# All the possible url paths related to login
urlpatterns = [
    path('', views.home, name='home'),
    path('login/', views.loginView, name='login'),
    path('signup/', views.signupView, name='signup')
]
