from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone


def user_directory_path(instance, filename):
    return 'items_images/user_{0}/{1}'.format(instance.id, filename)

# Creating the UserProfile model with its required attributes


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(upload_to='avatar/', default="avatar/defaultavatar.png")
    date_of_birth = models.DateField('date of birth')
    balance = models.DecimalField(default=0, max_digits=10, decimal_places=2)

# Creating the Blog model with its required attributes


class Blog(models.Model):
    entries = models.CharField(max_length=200)
    creation_date = models.DateTimeField('date created')
    last_modif_date = models.DateTimeField('date modified')
