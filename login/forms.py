from django import forms
from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

# Form to create a new user in the database


class SignUpForm(forms.Form):
    fname = forms.CharField(label="First Name:", max_length=200)
    lname = forms.CharField(label="Last Name:", max_length=200)
    date_of_birth = forms.DateTimeField(label='Date of birth')
    username = forms.CharField(label="Username:", max_length=200)
    password = forms.CharField(label="Password", widget=forms.PasswordInput)
    email = forms.EmailField(label="Email:", max_length=200)
    avatar = forms.ImageField(label="Avatar")

    class Meta:
        fields = ('fname', 'lname', 'date_of_birth', 'username', 'password', 'email')

# Form to login to the website with an account in the database


class LoginForm(forms.Form):
    username = forms.CharField(label="Username:", max_length=200, required=True)
    password = forms.CharField(label="Password:", max_length=200, required=True, widget=forms.PasswordInput)

    class Meta:
        fields = ('username', 'password')
